<?php

namespace Tests\Feature;

use App\Jobs\ProcessRegister;
use App\Register;
use App\Code;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    private Code $code;

    protected function setUp(): void
    {
        parent::setUp();

        $this->code = factory(Code::class)->create();
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_register_can_be_stored()
    {
        $this->withoutExceptionHandling();

        Queue::fake();

        $response = $this->post('/api/registers', $this->data());

        Queue::assertPushed(ProcessRegister::class);

        $response->assertStatus(Response::HTTP_CREATED);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_register_can_be_retrieved()
    {
        $register = factory(Register::class)->create(['code_id' => $this->code->id]);

        $response = $this->get('/api/registers/' . $register->id);

        $response->assertJson([
            'data' => [
                'register_id' => $register->id,
                'identifier' => $register->code->identifier,
                'mobile_number' => $register->mobile_number,
                'last_updated' => $register->updated_at->diffForHumans(),
            ]
        ]);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_register_can_be_updated()
    {
        $register = factory(Register::class)->create(['code_id' => $this->code->id]);

        $response = $this->patch('/api/registers/' . $register->id, $this->data());

        $register = $register->fresh();

        $this->assertCount(1, Register::all());
        $this->assertEquals('09127777777', $register->mobile_number);
        $this->assertEquals($this->code->identifier, $register->code->identifier);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            'data' => [
                'register_id' => $register->id,
            ]
        ]);
    }


    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_register_can_be_deleted()
    {
        $register = factory(Register::class)->create(['code_id' => $this->code->id]);

        $response = $this->delete('/api/registers/' . $register->id);

        $this->assertCount(0, Register::all());

        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_list_of_registers_can_be_retrieved()
    {
        $register = factory(Register::class)->create(['code_id' => $this->code->id]);
        factory(Register::class)->create(['code_id' => $this->code->id]);

        $this->assertCount(2, Register::all());

        $response = $this->get('/api/registers');

        //Considering pagination, the response has three elements
        $response->assertJsonCount(3)
            ->assertJson([
                'data' => [
                    [
                        'data' => [
                            'register_id' => $register->id
                        ],
                    ]
                ]
            ]);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function fields_are_required()
    {
        collect(['mobile_number', 'identifier'])
            ->each(function ($field) {
                $response = $this->post('/api/registers', array_merge($this->data(), [$field => '']));

                $response->assertSessionHasErrors($field);
                $this->assertCount(0, Register::all());
            });
    }

    /**
     * @return string[]
     */
    private function data(): array
    {
        return [
            'mobile_number' => '09127777777',
            'identifier' => $this->code->identifier,
        ];
    }
}
