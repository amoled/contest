<?php

namespace Tests\Feature;

use App\Code;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CodeTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_code_can_be_stored()
    {
        $response = $this->post('/api/codes', $this->data());

        $code = Code::first();

        $this->assertCount(1, Code::all());
        $this->assertEquals('123456', $code->identifier);
        $this->assertEquals(100, $code->limit);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson([
            'data' => [
                'code_id' => $code->id,
                'identifier' => $code->identifier,
                'limit' => $code->limit,
            ]
        ]);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_code_can_be_retrieved()
    {
        $code = factory(Code::class)->create();

        $response = $this->get('/api/codes/' . $code->id);

        $response->assertJson([
            'data' => [
                'code_id' => $code->id,
                'identifier' => $code->identifier,
                'limit' => $code->limit,
                'last_updated' => $code->updated_at->diffForHumans(),
            ]
        ]);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_code_can_be_updated()
    {
        $code = factory(Code::class)->create();

        $response = $this->patch('/api/codes/' . $code->id, $this->data());

        $code = $code->fresh();

        $this->assertCount(1, Code::all());
        $this->assertEquals('123456', $code->identifier);
        $this->assertEquals(100, $code->limit);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            'data' => [
                'code_id' => $code->id,
                'identifier' => $code->identifier,
                'limit' => $code->limit,
            ]
        ]);
    }


    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_code_can_be_deleted()
    {
        $code = factory(Code::class)->create();

        $response = $this->delete('/api/codes/' . $code->id);

        $this->assertCount(0, Code::all());

        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_list_of_codes_can_be_retrieved()
    {
        $code = factory(Code::class)->create();
        factory(Code::class)->create();

        $this->assertCount(2, Code::all());

        $response = $this->get('/api/codes');

        //Considering pagination, the response has three elements
        $response->assertJsonCount(3)
            ->assertJson([
                'data' => [
                    [
                        'data' => [
                            'code_id' => $code->id
                        ],
                    ]
                ]
            ]);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function fields_are_required()
    {
        collect(['identifier', 'limit'])
            ->each(function ($field) {
                $response = $this->post('/api/codes', array_merge($this->data(), [$field => '']));

                $response->assertSessionHasErrors($field);
                $this->assertCount(0, Code::all());
            });
    }


    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function an_identifier_should_be_unique()
    {
        $this->post('/api/codes', $this->data());
        $this->assertCount(1, Code::all());

        $response = $this->post('/api/codes', $this->data());
        $this->assertCount(1, Code::all());
        $response->assertSessionHasErrors();
    }


    /**
     * @return string[]
     */
    private function data(): array
    {
        return [
            'identifier' => '123456',
            'limit' => '100',
        ];
    }
}
