<?php

namespace App\Observers;

use App\Code;
use Illuminate\Support\Facades\Cache;

class CodeObserver
{
    /**
     * Handle the code "created" event.
     *
     * @param Code $code
     * @return void
     */
    public function created(Code $code)
    {
        $this->storeInCache();
    }

    /**
     * Handle the code "updated" event.
     *
     * @param Code $code
     * @return void
     */
    public function updated(Code $code)
    {
        $this->storeInCache();
    }

    /**
     * Handle the code "deleted" event.
     *
     * @param Code $code
     * @return void
     */
    public function deleted(Code $code)
    {
        $this->storeInCache();
    }

    /**
     * Handle the code "restored" event.
     *
     * @param Code $code
     * @return void
     */
    public function restored(Code $code)
    {
        $this->storeInCache();
    }

    /**
     * Handle the code "force deleted" event.
     *
     * @param Code $code
     * @return void
     */
    public function forceDeleted(Code $code)
    {
        $this->storeInCache();
    }

    private function storeInCache()
    {
        $codes = Code::pluck('id', 'identifier')->toArray();

        Cache::put('codes', $codes);
    }
}
