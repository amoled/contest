<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    protected $fillable = ['mobile_number', 'send_time'];

    //Define One to Many Inverse relationship between Register and Code models
    public function code()
    {
        return $this->belongsTo(Code::class);
    }

    //find model by code mobile number
    public function scopeMobileNumber($query, $mobileNumber)
    {
            $query->where('mobile_number', 'LIKE', '%' . $mobileNumber . '%');
    }

    public function isWinner()
    {
        return $this->code()->first()->winner->contains($this);
    }
}
