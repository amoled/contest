<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    protected $fillable = ['identifier', 'limit'];

    protected $dates = ['send_time'];

    //Define One to Many relationship between Code and Register models
    public function register()
    {
        return $this->hasMany(Register::class);
    }

    public function winner()
    {
        return $this->hasMany(Register::class)
            ->mobileNumber(request('mobile_number'))
            ->orderBy('send_time')
            ->take($this->limit);
    }

    //find model by code identifier
    public function scopeIdentifier($query, $identifier)
    {
        return $query->where('identifier', 'LIKE', '%' . $identifier . '%');
    }

    //return path to show this model
    public function path()
    {
        return '/codes/' . $this->id;
    }

}
