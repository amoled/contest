<?php

namespace App\Jobs;

use App\Code;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ProcessRegister implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    public $tries = 3;
    public $maxExceptions = 3;

    /**
     * Create a new job instance.
     *
     * @param array $data
     * @param string $time
     */
    public function __construct($data, $time)
    {
        $this->data = $data;
        $this->data['send_time'] = $time;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $code = Code::Identifier($this->data['identifier'])->firstOrFail();
            $code->register()->create($this->data);
        } catch (\Exception $e) {
            Log::error('cant process register: ' . $e->getMessage());
        }
    }
}
