<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class Register extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'register_id' => $this->id,
                'identifier' => $this->code->identifier,
                'mobile_number' => $this->mobile_number,
                'send_time' => $this->send_time,
                'is_winner' => $this->isWinner(),
                'last_updated' => $this->updated_at->diffForHumans(),
            ]
        ];
    }
}
