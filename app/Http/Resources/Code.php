<?php

namespace App\Http\Resources;

use App\Http\Resources\Register as RegisterResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Code extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'code_id' => $this->id,
                'identifier' => $this->identifier,
                'limit' => $this->limit,
                'used' => $this->register->count(),
                'winners' => RegisterResource::collection($this->winner),
                'last_updated' => $this->updated_at->diffForHumans(),
            ],
            'links' => [
                'self' => $this->path(),
            ]
        ];
    }
}
