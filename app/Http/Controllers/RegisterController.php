<?php

namespace App\Http\Controllers;

use App\Code;
use App\Jobs\ProcessRegister;
use App\Register;
use App\Http\Resources\Register as RegisterResource;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpFoundation\Response;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return RegisterResource
     */
    public function index()
    {
        $registers = Register::with('code')
            ->mobileNumber(request('mobile_number'))
            ->paginate();

        RegisterResource::collection($registers);

        return RegisterResource::collection($registers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $data = $this->validatedData();

        $codes = Cache::rememberForever('codes', function () {
            return Code::pluck('id', 'identifier')->toArray();
        });

        if (isset($codes[request('identifier')])) {
            ProcessRegister::dispatch($data, now())->delay(now()->addSeconds(5));

            return response(['data' => 'Your phone number registered successfully.'], Response::HTTP_CREATED);
        } else {
            return response(['data' => 'The code identifier is not valid.'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Register $register
     * @return RegisterResource
     */
    public function show(Register $register)
    {
        return new RegisterResource($register);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Code $code
     * @return \Illuminate\Http\Response
     */
    public function edit(Code $code)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Register $Register
     * @return \Illuminate\Http\Response
     */
    public function update(Register $Register)
    {
        $data = $this->validatedData($Register->id);

        $Register->update($data);

        return (new RegisterResource($Register))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Register $Register
     * @return \Illuminate\Http\Response
     */
    public function destroy(Register $Register)
    {
        $Register->delete();

        return response(['data' => 'Successfully deleted the registered code'], Response::HTTP_NO_CONTENT);
    }

    private function validatedData($RegisterId = null)
    {
        return request()->validate([
            'identifier' => 'required',
            'mobile_number' => 'required|unique:registers,mobile_number,' . $RegisterId,
        ]);
    }
}
