<?php

namespace App\Http\Controllers;

use App\Code;
use App\Http\Resources\Code as CodeResource;
use Symfony\Component\HttpFoundation\Response;

class CodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return CodeResource
     */
    public function index()
    {
        $codes = Code::with('register')
            ->identifier(request('identifier'))
            ->paginate();

        return CodeResource::collection($codes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $code = Code::create($this->validatedData());

        return (new CodeResource($code))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Code $code
     * @return \Illuminate\Http\Response
     */
    public function show(Code $code)
    {
        return new CodeResource($code);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Code $code
     * @return \Illuminate\Http\Response
     */
    public function edit(Code $code)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Code $code
     * @return \Illuminate\Http\Response
     */
    public function update(Code $code)
    {
        $data = $this->validatedData($code->id);

        $code->update($data);

        return (new CodeResource($code))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Code $code
     * @return \Illuminate\Http\Response
     */
    public function destroy(Code $code)
    {
        $code->delete();

        return response(['data' => 'Successfully deleted the code'], Response::HTTP_NO_CONTENT);
    }

    private function validatedData($codeId = null)
    {
        return request()->validate([
           'identifier' => 'required|unique:codes,identifier,' . $codeId,
           'limit' => 'required|numeric'
        ]);
    }
}
