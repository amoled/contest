<?php

use Illuminate\Support\Facades\Route;


Route::resource('codes', 'CodeController');
Route::resource('registers', 'RegisterController');

