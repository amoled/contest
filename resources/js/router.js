import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeComponent from "./components/HomeComponent";
import CodeIndex from "./views/CodeIndex";
import CodeCreate from "./views/CodeCreate";
import CodeShow from "./views/CodeShow";
import CodeEdit from "./views/CodeEdit";
import RegisterIndex from "./views/RegisterIndex";
import RegisterShow from "./views/RegisterShow";



Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        { path: '/', component: HomeComponent},
        { path: '/codes', component: CodeIndex},
        { path: '/codes/create', component: CodeCreate},
        { path: '/codes/:id', component: CodeShow},
        { path: '/codes/:id/edit', component: CodeEdit},
        { path: '/registers', component: RegisterIndex},
        { path: '/registers/:id', component: RegisterShow},
    ],
    mode: 'history'
})
