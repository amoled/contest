<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Code;
use App\Register;
use Faker\Generator as Faker;

$factory->define(Register::class, function (Faker $faker) {
    return [
        'code_id' => factory(Code::class)->create(),
        'mobile_number' => $faker->phoneNumber,
        'send_time' => now(),
    ];
});
