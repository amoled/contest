<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Code;
use Faker\Generator as Faker;

$factory->define(Code::class, function (Faker $faker) {
    return [
        'identifier' => $faker->numberBetween(10000, 99999),
        'limit' => $faker->numberBetween(100,1000),
    ];
});
