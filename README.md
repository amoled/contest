# Contest

A simple API created with Laravel framework to define limited codes used by customers to participate in a tense contest. It has an admin panel created by Vue.js to manage codes and find winners.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

This project uses REDIS for caching and manging queues. Just make sure that the php redis extension is enabled.


### Installing

After cloning this project you should get the required packages, and run migrations.

run these commands in order (seeding is optional):

```
composer install
php artisan migrate
php artisan db:seed
php artisan optimize
```

Then run this command 

```
php artisan serve
```

next, open _localhost:8000_ in your browser.


## Running the queue

In order to process code registers, you should keep the queue running:
```
php artisan queue:work
```

## Running the tests

This project contains feature tests for Code and Register models, to run these tests:

```
php artisan config:clear
vendor\bin\phpunit
```
